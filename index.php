<?php
require_once 'functions.php';
require_once 'vendor/autoload.php';
header('Content-Type:text/html; charset=utf-8');
include 'Contact.php';

$loader = new \Twig\Loader\FilesystemLoader('templates');
$twig = new \Twig\Environment($loader);

if (isset($_GET['command'])) {
    $command = $_GET['command'];
} else {
    $command = 'none';
}

if (isset($_GET['message'])) {
    $message = $_GET['message'];
} else {
    $message = 'none';
}
$content = "";
if ($command == "save") {
    $contact = new Contact();
    $contact->create($_POST);
    header('Location:?cmd=list_page&message=saved');
} else if ($command == 'show_add_page') {
    $content = $twig->render('add.html');
    $title = 'Lisa kontakt';
    echo $twig->render('main.html',
        ['links' => array('list' => 'Nimekiri', 'add' => 'Lisa kontakt')
            , 'title' => $title]);
    echo $content;
    echo $twig->render('footer.html');
} else if ($command == 'show_list_page' || $command == 'none') {
    if($message == 'saved'){
        $content = $twig->render('saved.html');
    }
    $contact = new Contact();
    $userData = $contact->getAllContact();
    $content .= $twig->render('list.html', ['userData' => $userData]);
    $title = 'Nimekiri';

    echo $twig->render('main.html',
        ['links' => array('list' => 'Nimekiri', 'add' => 'Lisa kontakt')
            , 'title' => $title]);
    echo $content;
    echo $twig->render('footer.html');
}