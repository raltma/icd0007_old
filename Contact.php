<?php
include "database.php";

class Contact
{

    public function create($userData) {

        $connection = connectDb();
        $stmt = "INSERT INTO contacts (first_name, last_name) VALUES (:firstName, :lastName) ";
        $stmt = $connection->prepare($stmt);
        $stmt->bindParam(":firstName", $userData['firstName']);
        $stmt->bindParam(":lastName", $userData['lastName']);
        $stmt->execute();
        $contactId = $connection->lastInsertId();
        $phones = [$userData['phone1'], $userData['phone2'], $userData['phone3']];
        foreach ($phones as $phone) {
            $stmt = "INSERT INTO phone_numbers (number, contact_id) VALUES (:number, :contact_id) ";
            $stmt = $connection->prepare($stmt);
            $stmt->bindParam(":number", $phone);
            $stmt->bindParam(":contact_id", $contactId);
            $stmt->execute();
        }
    }

    public function getAllContact() {
        $fullContacts = [];

        $connection = connectDb();
        $contact = "SELECT contact_id, first_name, last_name  from contacts";
        $contact = $connection->prepare($contact);
        if ($contact->execute()) {
            $contact = $contact->fetchAll(PDO::FETCH_ASSOC);
            foreach ($contact as $con) {
                $phones = "SELECT number from phone_numbers 
                            where contact_id = " . $con['contact_id'];
                $phones = $connection->prepare($phones);
                $phones->execute();
                $phones = $phones->fetchAll(PDO::FETCH_ASSOC);
                $phoneString = "";
                foreach ($phones as $phone) {
                    if ($phone['number']) {
                        $phoneString .= $phone['number'] . ", ";
                    }
                }
                $phoneString = substr($phoneString, 0, -2);
                array_push(
                    $fullContacts,
                    [$con['first_name'],
                    $con['last_name'],
                    $phoneString]
                );
            }
        }
        return $fullContacts;

    }


}